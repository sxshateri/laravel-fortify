@extends('template')

@section('content')
    <div class="d-flex row justify-content-center mt-5">
        <form method="POST" action="{{ route('login') }}">
            @csrf
            <div class="form-group">
            <label for="login-email">Email address</label>
            <input type="email" class="form-control @error('email') is-invalid @enderror" id="login-email" aria-describedby="emailHelp" name="email">
            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
            </div>
            <div class="form-group">
            <label for="login-password">Password</label>
            <input type="password" class="form-control @error('password') is-invalid @enderror" id="login-password" name="password">
            </div>
            <div class="my-4"><a href="{{route('register')}}">Register new user</a></div>
            <button type="submit" class="btn btn-primary">Login</button>
        </form>
    </div>
@endsection

