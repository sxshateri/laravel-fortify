@extends('template')

@section('content')
    <div class="d-flex row justify-content-center mt-5">
        <form method="POST" action="{{ route('register') }}">
            @csrf
            <div class="form-group">
                <label for="register-name">Name</label>
                <input type="text" class="form-control @error('name') is-invalid @enderror" id="register-name" name="name">
                @error('name')
                    <span class="invalid-feedback is-invalid" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="register-email">Email address</label>
                <input type="email" class="form-control @error('email') is-invalid @enderror" id="register-email" name="email" autocomplete="username">
                <small id="register-email" class="form-text text-muted">We'll never share your email with anyone else.</small>
                @error('email')
                    <span class="invalid-feedback is-invalid" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="register-password">Password</label>
                <input type="password" class="form-control @error('password') is-invalid @enderror" id="register-password" name="password" autocomplete="new-password">
                @error('password')
                    <span class="invalid-feedback is-invalid" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="register-password-confirmation">Password</label>
                <input type="password" class="form-control @error('password') is-invalid @enderror" id="register-password-confirmation" name="password_confirmation" autocomplete="new-password">
            </div>

            <button type="submit" class="btn btn-primary">{{ __('Register') }}</button>
        </form>
    </div>
@endsection

